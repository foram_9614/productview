<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>My Cart</title>
<link rel="stylesheet" href="css/styletable.css">

</head>
<body>
	<div class="wrap" style="height: 725px">
		<div class="html">
			<c:choose>
				<c:when test="${not empty cart}">
					<div class="table-users">
						<div class="form">
							<div class="group">
								<div class="header">Your Cart is Here</div>

								<table style="width: 100%" cellspacing="0"
									style="text-color:#000000">
									<tr>

										<th>Name</th>
										<th>Price</th>
										<th>Quantity</th>
										<th>Sub Total</th>
										<th></th>


									</tr>

									<c:set var="s" value="0"></c:set>
									<c:forEach var="pr" items="${cart}">
										<c:set var="s" value="${s+ pr.productPrice * pr.quantity}"></c:set>
										<tr>

											<td>${pr.productName }</td>
											<td>${pr.productPrice }</td>
											<td>${pr.quantity}</td>
											<td>${pr.productPrice * pr.quantity}</td>
											<td><form action="delete" method="get">
													<input type="hidden" name="productId"
														value="${pr.productId}" /><input type="hidden"
														name="emailId" value="${emailId}" /> <input type="Submit"
														value="Remove From cart" class="button" />
												</form></td>

										</tr>
									</c:forEach>
									<tr>
										<td colspan="4" align="right">Sum</td>
										<td>${s}</td>

									</tr>
								</table>
							</div>

							<div class="group">
								<form action="getAllProducts" method="get">
								<input type="hidden" name="emailId" value="${emailId}" />
									<input type="Submit" value="Buy More" class="button" />
								</form>
							</div>
							<br>
							<div class="group">
								<form action="clearCart" method="get">
								<input type="hidden" name="emailId" value="${emailId}" />
									<input type="Submit" value="Clear cart" class="button" />
								</form>
							</div>
							<br>
							<div class="group">
								<form action="processOrder" method="get">
								<input type="hidden" name="emailId" value="${emailId}" />
									<input type="submit" style="width: 300px; margin: 0 auto;"
										value="CheckOut" class="button">
								</form>
							</div>
						</div>
					</div>

				</c:when>

				<c:otherwise>
					<div class="form">
						<div class="group" style="margin-left: 350px">
							<label for="user" class="label" style="font-size: 50px">Your
								Cart is Empty</label>
						</div>


						<div class="group" style="margin-top: 50px">
							<form action="getAllProducts" method="get">
							<input type="hidden" name="emailId" value="${emailId}" />
								<input type="submit" style="width: 300px; margin: 0 auto;"
									value="Buy More" class="button">
							</form>
						</div>

					</div>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</body>
</html>