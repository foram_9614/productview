<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
table, th, td {
	border: 1px solid black;
	border-collapse: collapse;
}
</style>
</head>
<body>
	<h1>Your Shopping cart</h1>
	<c:choose>
		<c:when test="${not empty check}">
			<table style="width: 100%">
				<tr>
					<th>Product Id</th>
					<th>Product Name</th>
					<th>Product Price</th>
					<th>Product quantity</th>

				</tr>
				<c:if test="${not empty check}">
					<c:forEach var="orderitems" items="${check}">
						<tr>
							<td><c:out value="${orderitems.productId}" /></td>
							<td><c:out value="${orderitems.productName}" /></td>
							<td><c:out value="${orderitems.productPrice}" /></td>
							<td><c:out value="${orderitems.quantity}" /></td>
						</tr>
					</c:forEach>
					<br>
					<br>

					<div style="text-align: center">
						<form action="email" method="get">
							<input type="submit" style="width: 300px; margin: 0 auto;"
								value="CheckOut">
						</form>
					</div>
				</c:if>
			</table>
		</c:when>
		<c:otherwise>Your cart is empty
		
		<br>
			<br>

			<div style="text-align: center">
				<form action="getAllProducts" method="get">
					<input type="submit" style="width: 300px; margin: 0 auto;"
						value="Buy More">
				</form>
			</div>
		</c:otherwise>
	</c:choose>





</body>
</html>