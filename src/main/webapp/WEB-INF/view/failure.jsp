<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Quantity Does Not Exist</title>
<link rel="stylesheet" href="css/styletable.css">

</head>
<body>

	<div class="wrap" style="height: 725px">
		<div class="html">
			<div class="form">

				<c:choose>
					<c:when test="${quantity <= 0}">
						<div class="group" style="margin-left: 215px">
							<label for="user" class="label" style="font-size: 50px">Sorry
								${quantity} items cant be added, try <br /> <br /> <br /> <br />adding
								more items
							</label>
						</div>


					</c:when>
					<c:otherwise>
						<div class="group" style="margin-left: 215px">
							<label for="user" class="label" style="font-size: 50px">Sorry
								${quantity} items cant be added, try <br /> <br /> <br /> <br />
								adding less items
							</label>
						</div>
					</c:otherwise>
				</c:choose>


				<div class="group"
					style="margin-top: 50px; margin-left: 215px; width: 775px">
					<form action="getAllProducts" method="get">
						<input type="hidden" name="emailId" value="${emailId}" /> <input
							type="Submit" value="Buy Again" class="button" />
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>