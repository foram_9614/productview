<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Thank You</title>
<link rel="stylesheet" href="css/styletable.css">
</head>
<body>
	<div class="wrap" style="height: 725px">
		<div class="html">
			<div class="form">
				<div class="group" style="margin-left: 200px">
					<label for="user" class="label" style="font-size: 50px">Thank
						You for Shopping With Us</label>
				</div>
				<div class="group"
					style="margin-top: 50px; margin-left: 200px; width: 775px">
					<form action="getAllProducts" method="get">
						<input type="hidden" name="emailId" value="${emailId}" /> <input
							type="Submit" value="Buy more" class="button" />
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>