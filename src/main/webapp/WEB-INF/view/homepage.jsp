<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



<link rel="stylesheet" href="css/styletable.css">
<title>CoviamStore</title>
<title>Products</title>
</head>
<body>
	<div class="wrap">
		<div class="html">
			<form action="/search">

				<div class="form">
					<div class="group"
						style="margin-left: 175px; width: 380px; margin-bottom: -49px">
						<input type="text" class="input" name="productCategory" required
							placeholder="Search by Product/Category"> <input
							type="hidden" name="emailId" value="${emailId}" />


					</div>
					<div class="group" style="width: 380px; margin-left: 590px">

						<input type="submit" class="button" value="Search">

					</div>

				</div>
			</form>

			<%--  <form action="/searchByProduct">
        Search by Name: <input type="text" name="productName" required> <input type="hidden" name="emailId" value="${ emailId}" /> <input
            type="submit" value="Search">
    </form> --%>
			<div class="table-users" style="margin-top: -250px">
				<div class="form">
					<div class="group">
						<div class="header">Coviam Store</div>

						<table style="width: 100%" cellspacing="0"
							style="text-color:#000000">
							<tr>
								<th>Product Category</th>
								<th>Product Name</th>
								<th colspan="2" align="left">Product Price</th>


							</tr>
							<c:if test="${not empty productList}">
								<c:forEach var="product" items="${productList}">
									<tr>
										<td><c:out value="${product.productCategory}" /></td>
										<td><c:out value="${product.productName}" /></td>
										<td><c:out value="${product.productPrice}" /></td>

										<td><form action="checkForBuy" method="get">
												<input type="hidden" name="productId"
													value="${product.productID}" /> <input type="hidden"
													name="emailId" value="${emailId}" /><input type="submit"
													value="AddToCart" class="button" data-toogle="modal"
													data-target="#quantityModal">
											</form></td>
									</tr>
								</c:forEach>
							</c:if>
						</table>
						<form action="showCart" method="get">
							<input type="hidden" name="emailId" value="${emailId}" /> <input
								type="submit" class="button" value="Show Cart"
								style="margin-top: 50px">

						</form>

					</div>

				</div>



			</div>
		</div>
	</div>


</body>
</html>




