package com.store.view.threadsafe;

import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.store.entity.vo.CartItems;

public class ProcessThread implements Runnable{

	String url;
	boolean flag;
	CartItems[] cart;
	
	
	
	public ProcessThread(String url, CartItems[] cart) {
		super();
		this.url = url;
		this.cart = cart;
	}



	@Override
	public void run() {
		// TODO Auto-generated method stub
		RestTemplate restTemplate = new RestTemplate();
		
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
		
		restTemplate.postForObject(url, cart, Void.class);
		
	}

}
