package com.store.view.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpSession;

import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.client.RestTemplate;

import com.store.entity.vo.CartItems;
import com.store.entity.vo.ProductVO;
import com.store.view.threadsafe.ProcessThread;

@Controller
// @SessionAttributes("emailId")
public class ViewController {

	private static String productController = "http://localhost:8080/";
	private static String orderController = "http://localhost:8082/";
	private static String cartController = "http://localhost:8083/";

	ExecutorService executorService = Executors.newFixedThreadPool(2);

	@RequestMapping("/")
	public String showHome() {
		return "emailForm";
	}

	/*@RequestMapping("/getProducts")
	public String showProducts(@RequestParam("emailId") String emailId, ModelMap map) {
		// httpSession.setAttribute("emailId", emailId);
		return "redirect:/getAllProducts";
	}*/

	@SuppressWarnings("unchecked")
	@RequestMapping("/getAllProducts")
	public String showAllProducts(@RequestParam("emailId") String emailId, ModelMap map) {
		String url = productController + "getAllProducts";
		RestTemplate restTemplate = new RestTemplate();
		List<ProductVO> productList = restTemplate.getForObject(url, List.class);
		map.addAttribute("productList", productList);
		map.addAttribute("emailId", emailId);
		return "homepage";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/search")
	public String searchProduct(@RequestParam("productCategory") String productCategory,
			@RequestParam("emailId") String emailId, ModelMap map) {
		String searchurl = productController + "search/" + productCategory;
		RestTemplate restTemplate = new RestTemplate();
		List<ProductVO> productList = restTemplate.getForObject(searchurl, List.class);
		map.addAttribute("productList", productList);
		map.addAttribute("emailId", emailId);
		return "homepage";
	}

	@RequestMapping("/checkForBuy")
	public String checkFromTable(@RequestParam("productId") Integer productId, @RequestParam("emailId") String emailId,
			ModelMap map) {
		map.addAttribute("productId", productId);
		map.addAttribute("emailId", emailId);
		return "checkForQuantityForm";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/check")
	public String checkProductQuantity(@RequestParam("productId") Integer productId,
			@RequestParam("emailId") String emailId, @RequestParam("quantity") Integer quantity, ModelMap map) {

		String checkurl = productController + "checkQuantity/" + productId + "/" + quantity;
		RestTemplate restTemplate = new RestTemplate();
		boolean check = restTemplate.getForObject(checkurl, boolean.class);

		// String emailId = (String) httpSession.getAttribute("emailId");

		if (check) {
			String prodUrl = productController + "getProductById/" + productId;

			restTemplate = new RestTemplate();
			ProductVO productVO = restTemplate.getForObject(prodUrl, ProductVO.class);
			CartItems cartItems = new CartItems();
			cartItems.setProductName(productVO.getProductName());
			cartItems.setProductPrice(productVO.getProductPrice());
			cartItems.setProductId(productId);
			cartItems.setQuantity(quantity);

			String carturl = cartController + "addToCart/" + emailId;
			restTemplate = new RestTemplate();
			restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
			restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
			boolean cartCheck = restTemplate.postForObject(carturl, cartItems, boolean.class);
			if (cartCheck) {
				String url = productController + "getAllProducts";
				restTemplate = new RestTemplate();
				List<ProductVO> productList = restTemplate.getForObject(url, List.class);
				map.addAttribute("productList", productList);
				map.addAttribute("emailId", emailId);
				return "homepage";
			} else
				return "unprocess";
		} else {
			map.addAttribute("emailId", emailId);
			map.addAttribute("quantity", quantity);
			return "failure";
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/delete")
	public String deleteFromCart(@RequestParam("productId") Integer productId, @RequestParam("emailId") String emailId,
			ModelMap map) {
		// String emailId = (String) httpSession.getAttribute("emailId");
		String checkurl = cartController + "delete/" + productId + "/" + emailId;

		RestTemplate restTemplate = new RestTemplate();
		List<CartItems> cart = restTemplate.getForObject(checkurl, List.class);
		map.addAttribute("cart", cart);
		map.addAttribute("emailId", emailId);
		return "cart";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/showCart")
	public String showCart(@RequestParam("emailId") String emailId, ModelMap map) {
		// String emailId = (String) httpSession.getAttribute("emailId");
		String url = cartController + "showCart/" + emailId;
		RestTemplate restTemplate = new RestTemplate();

		List<CartItems> cart = restTemplate.getForObject(url, List.class);
		map.addAttribute("cart", cart);
		map.addAttribute("emailId", emailId);
		return "cart";

	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/clearCart")
	public String clearCart(@RequestParam("emailId") String emailId, ModelMap map) {

		// String emailId = (String) httpSession.getAttribute("emailId");
		String url = cartController + "clearCart/" + emailId;

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getForObject(url, Void.class);
		String bookurl = productController + "getAllProducts";
		restTemplate = new RestTemplate();
		List<ProductVO> productList = restTemplate.getForObject(bookurl, List.class);
		map.addAttribute("productList", productList);
		map.addAttribute("emailId", emailId);
		return "homepage";
	}

	private static final Map<Integer, String> PROCESS_ORDER = new HashMap<>();
	static {

		PROCESS_ORDER.put(1, productController + "buyProduct/");
		PROCESS_ORDER.put(2, orderController + "processingOrder/");
	}

	@RequestMapping("/processOrder")
	public String processOrder(@RequestParam("emailId") String emailId, ModelMap map) {

		// String emailId = (String) httpSession.getAttribute("emailId");
		String getCarturl = cartController + "showCart/" + emailId;

		RestTemplate restTemplate = new RestTemplate();
		CartItems[] cart = restTemplate.getForObject(getCarturl, CartItems[].class);

		for (int i = 1; i < 3; i++) {
			Runnable runnable = new ProcessThread(PROCESS_ORDER.get(i) + emailId, cart);
			executorService.execute(runnable);
		}

		executorService.shutdown();
		try {
			executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		String clearCart = cartController + "clearCart/" + emailId;
		restTemplate = new RestTemplate();
		restTemplate.getForObject(clearCart, Void.class);
		map.addAttribute("emailId", emailId);
		return "thankyou";

	}
}
